/**
 * Settimeout using physics world time.
 */

type Timeout = {
  callback: Function;
  delta: number;
  duration: number;
};

let timeouts: Timeout[] = [];

export function setWorldTimeout(callback: Function, duration: number) {
  timeouts.push({
    callback,
    delta: 0,
    duration,
  });
}

export function updateTimeout(delta: number) {
  timeouts = timeouts.reduce((accumulator, timeout) => {
    // eslint-disable-next-line no-param-reassign
    timeout.delta += delta;
    if (timeout.delta >= timeout.duration) {
      timeout.callback();
      return accumulator;
    }
    return [...accumulator, timeout];
  }, [] as Timeout[]);
}
