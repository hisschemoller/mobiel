import { ExtendedObject3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';

export type TricycleConfig = {
	frontWheelsRadius: number;
	backWheelRadius: number;
	bodyLength: number;
	position: THREE.Vector3;
	rotateY: number;
	velocity: number;
}

type TricycleParams = {
	body: ExtendedObject3D;
}

export default class Tricycle {

	constructor (asyncParam: TricycleParams | undefined) {
		if (typeof asyncParam === 'undefined') {
			throw new Error('Cannot be called directly');
		}
	}

	static async build(
		projectSettings: ProjectSettings,
		texture: THREE.Texture,
		config: TricycleConfig,
	) {
		const { scene3d } = projectSettings;
		const {
			frontWheelsRadius,
			backWheelRadius,
			bodyLength,
			position,
			rotateY,
			velocity,
		} = config;
		const axisRadius = 0.05;
		const frontAxisLength = 1;
		const backAxisLength = 0.1;
		const wheelWidth = 0.06;
		const bodyDepth = 0.06;
		const bodyHeight = 0.2;

		const group = new THREE.Group();
		group.rotateY(rotateY);
		group.position.add(position);
		scene3d.scene.add(group);

		// BODY
		const body = scene3d.add.box({
			depth: bodyDepth,
			height: bodyHeight,
			width: bodyLength,
		}, {
			lambert: { map: texture },
		});
		body.position.set((bodyLength / 2) - (bodyHeight / 2), 0, 0);
		group.add(body);
		scene3d.physics.add.existing(body, { mass: 0.1 });

		// FRONT AXIS
		const frontAxis = scene3d.add.cylinder({
			height: frontAxisLength - wheelWidth,
			radiusBottom: axisRadius,
			radiusTop: axisRadius,
		}, {
			lambert: { map: texture },
		});
		frontAxis.position.set(0, 0, 0);
		frontAxis.rotateX(Math.PI * 0.5);
		group.add(frontAxis);
		scene3d.physics.add.existing(frontAxis, { mass: 0.1 });

		// FRONT WHEEL LEFT
		const frontWheelLeft = scene3d.add.cylinder({
			x: 0,
			y: 0,
			z: frontAxisLength * 0.5,
			height: wheelWidth,
			radiusBottom: frontWheelsRadius,
			radiusTop: frontWheelsRadius,
		}, {
			lambert: { map: texture },
		});
		frontWheelLeft.rotateX(Math.PI * -0.5);
		group.add(frontWheelLeft);
		scene3d.physics.add.existing(frontWheelLeft, { mass: 0.1 });

		// FRONT WHEEL RIGHT
		const frontWheelRight = scene3d.add.cylinder({
			x: 0,
			y: 0,
			z: frontAxisLength * -0.5,
			height: wheelWidth,
			radiusBottom: frontWheelsRadius,
			radiusTop: frontWheelsRadius,
		}, {
			lambert: { map: texture },
		});
		frontWheelRight.rotateX(Math.PI * -0.5);
		group.add(frontWheelRight);
		scene3d.physics.add.existing(frontWheelRight, { mass: 0.1 });

		// BACK WHEEL
		const backWheel = scene3d.add.cylinder({
			x: bodyLength - (bodyHeight / 2),
			y: 0,
			z: backAxisLength,
			height: wheelWidth,
			radiusBottom: backWheelRadius,
			radiusTop: backWheelRadius,
		}, {
			lambert: { map: texture },
		});
		backWheel.rotateX(Math.PI * -0.5);
		group.add(backWheel);
		scene3d.physics.add.existing(backWheel, { mass: 0.1 });

		// CONSTRAINTS

		// FRONT LEFT WHEEL TO AXIS
		scene3d.physics.add.constraints.lock(frontWheelLeft.body, frontAxis.body);

		// FRONT RIGHT WHEEL TO AXIS
		scene3d.physics.add.constraints.lock(frontWheelRight.body, frontAxis.body);

		// BODY TO FRONT AXIS
		const motor = scene3d.physics.add.constraints.hinge(body.body, frontAxis.body, {
			pivotA: { x: (bodyLength / -2) + (bodyHeight / 2) },
			pivotB: { },
			axisA: { z: 1 },
			axisB: { y: 1 },
		});
		motor.enableAngularMotor(true, velocity, velocity * 0.1);

		// BODY TO BACK WHEEL
		scene3d.physics.add.constraints.hinge(body.body, backWheel.body, {
			pivotA: { x: (bodyLength / 2) - (bodyHeight / 2), z: backAxisLength },
			pivotB: { },
			axisA: { z: 1 },
			axisB: { y: -1 },
		});
	}
}
