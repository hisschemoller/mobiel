import { ProjectSettings } from '@app/interfaces';

let motor: Ammo.btHingeConstraint;

export default async function createObjects(projectSettings: ProjectSettings) {
	const { scene3d } = projectSettings;

	// scene3d.physics.setGravity(0, 0, 0);

	const axisWidth = 1.5;
	const wheelWidth = 0.1;
	const texture = await scene3d.load.texture('grid');

	scene3d.physics.add.box({
		collisionFlags: 1,
		depth: 10,
		height: 0.1,
		width: 10,
		y: -2,
	}, { lambert: { map: texture } });

	const axis = scene3d.add.cylinder({
		x: 0,
		height: axisWidth,
		radiusBottom: 0.1,
		radiusTop: 0.1,
	}, {
		lambert: { map: texture },
	});
	axis.rotateZ(Math.PI * 0.5);
	scene3d.physics.add.existing(axis, { mass: 0.1 });

	const wheelLeft = scene3d.add.cylinder({
		x: ((axisWidth / 2) + (wheelWidth / 2)) * -1,
		height: wheelWidth,
		radiusBottom: 1,
		radiusTop: 1,
	}, {
		lambert: { map: texture },
	});
	wheelLeft.rotateZ(Math.PI * 0.5);
	scene3d.physics.add.existing(wheelLeft, { mass: 0.1 });

	const wheelRight = scene3d.add.cylinder({
		x: ((axisWidth / 2) + (wheelWidth / 2)),
		height: wheelWidth,
		radiusBottom: 0.7,
		radiusTop: 0.7,
	}, {
		lambert: { map: texture },
	});
	wheelRight.rotateZ(Math.PI * 0.5);
	scene3d.physics.add.existing(wheelRight, { mass: 0.1 });

	motor = scene3d.physics.add.constraints.hinge(wheelLeft.body, axis.body, {
		pivotA: { y: wheelWidth * -0.5 },
		pivotB: { y: axisWidth * 0.5 },
		axisA: { y: 1 },
		axisB: { y: 1 }
	});
	motor.enableAngularMotor(true, 5, 0.5);

	scene3d.physics.add.constraints.fixed(wheelRight.body, axis.body)
}

export function updateObjects() {

}
