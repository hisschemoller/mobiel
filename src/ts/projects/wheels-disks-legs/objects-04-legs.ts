import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import WheelDouble from './wheel-double';
import WheelLegs from './wheel-legs';
import WheelStepper from './wheel-stepper';
import Dog from './dog';
import Tricycle from './tricycle';
import OtherLegs from './other-legs';

function createFloor(scene3d: MainScene, texture: THREE.Texture) {
	const SIZE = 20;
	const HEIGHT = 3;
	const Y = -2;

	// FLOOR
	const floor = scene3d.physics.add.box({
		collisionFlags: 1,
		depth: SIZE,
		height: 0.1,
		width: SIZE,
		y: Y,
	}, { lambert: { map: texture } });

	[
		{ x: 0, z: SIZE / -2, ry: 0 },
		{ x: SIZE / 2, z: 0, ry: Math.PI / 2 },
		{ x: 0, z: SIZE / 2, ry: Math.PI },
		{ x: SIZE / -2, z: 0, ry: Math.PI / -2 },
	].forEach(({ x, z, ry }) => {
		const wall = scene3d.add.box({
			depth: 0.1,
			height: HEIGHT,
			width: SIZE,
			x,
			y: Y + (HEIGHT / 2),
			z,
		}, { lambert: { map: texture } });
		wall.rotateY(ry);
		scene3d.physics.add.existing(wall, { collisionFlags: 1 });
	});

	return floor;
}

export default async function createObjects(projectSettings: ProjectSettings) {
	const { scene3d } = projectSettings;
	const texture = await scene3d.load.texture('grid');

	// scene3d.physics.setGravity(0, 0, 0);

	scene3d.physics.debug?.enable();

	createFloor(scene3d, texture);

	await WheelDouble.build(
		projectSettings,
		texture,
		new THREE.Vector3(-3, 0, -2),
		Math.PI * 0.3,
	);

	await WheelLegs.build(
		projectSettings,
		texture,
		{
			limbDepth: 0.3,
			limbWidth: 0.5,
			lowerHeight: 0.7,
			position: new THREE.Vector3(3, -1, -3),
			rotateY: Math.PI * 0.25,
			upperHeight: 0.8,
			velocity: 1,
			wheelRadius: 1,
			wheelWidth: 0.3,
		},
	);

	await WheelLegs.build(
		projectSettings,
		texture,
		{
			limbDepth: 0.3,
			limbWidth: 0.1,
			lowerHeight: 1.5,
			position: new THREE.Vector3(3, -1, 1),
			rotateY: 0,
			upperHeight: 1.8,
			velocity: 7,
			wheelRadius: 0.15,
			wheelWidth: 0.6,
		},
	);

	await WheelLegs.build(
		projectSettings,
		texture,
		{
			limbDepth: 0.3,
			limbWidth: 0.1,
			lowerHeight: 0.7,
			position: new THREE.Vector3(-2, -1, -3),
			rotateY: 0,
			upperHeight: 0.8,
			velocity: 12,
			wheelRadius: 0.2,
			wheelWidth: 0.3,
		},
	);

	await WheelStepper.build(
		projectSettings,
		texture,
		{
			limbDepth: 0.3,
			limbWidth: 0.1,
			lowerHeight: 0.9,
			position: new THREE.Vector3(2, -1, -5),
			rotateY: Math.PI * 0.2,
			upperHeight: 0.7,
			velocity: 1,
			wheelRadius: 0.3,
			wheelWidth: 0.2,
		},
	);

	await Dog.build(
		projectSettings,
		texture,
		{
			legHeight: 0.7,
			limbDepth: 0.3,
			limbWidth: 0.1,
			position: new THREE.Vector3(4, -1, -4),
			rotateY: Math.PI * 0.3,
			velocity: 8,
		},
	);

	await Tricycle.build(
		projectSettings,
		texture,
		{
			frontWheelsRadius: 0.4,
			backWheelRadius: 0.7,
			bodyLength: 1.0,
			position: new THREE.Vector3(4, 1, 4),
			rotateY: Math.PI * -0.3,
			velocity: 12,
		},
	);

	await OtherLegs.build(
		projectSettings,
		texture,
		{
			limbRadius: 0.05,
			lowerHeight: 1.5,
			position: new THREE.Vector3(0, 0, 0),
			rotateY: 0,
			upperHeight: 1.5,
			velocity: 20,
			wheelRadius: 0.8,
			wheelWidth: 0.02,
		},
	);
}

export function updateObjects() {}
