import { ExtendedObject3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';

type Wheel = {
	hinge: Ammo.btHingeConstraint,
	wheel: ExtendedObject3D,
}

const AXIS_RADIUS = 0.05;
const POLE_RADIUS = 0.03;

function createConnector(
	scene3d: MainScene,
	texture: THREE.Texture,
	wheel1: Wheel,
	wheel2: Wheel,
	wheel1Pivot: THREE.Vector3,
	wheel2Pivot: THREE.Vector3,
	) {
	// wheel1 axis
	const axis1Height = wheel1Pivot.y - wheel1.wheel.position.y;
	const wheel1PivotAxis = scene3d.add.cylinder({
		x: wheel1Pivot.x - wheel1.wheel.position.x,
		y: axis1Height / 2,
		z: wheel1Pivot.z - wheel1.wheel.position.z,
		height: axis1Height,
		radiusBottom: AXIS_RADIUS,
		radiusTop: AXIS_RADIUS,
	}, {
		lambert: { map: texture },
	});
	wheel1.wheel.add(wheel1PivotAxis);

	// wheel2 axis
	const axis2Height = wheel2Pivot.y - wheel2.wheel.position.y;
	const wheel2PivotAxis = scene3d.add.cylinder({
		x: wheel2Pivot.x - wheel2.wheel.position.x,
		y: axis2Height / 2,
		z: wheel2Pivot.z - wheel2.wheel.position.z,
		height: axis2Height,
		radiusBottom: AXIS_RADIUS,
		radiusTop: AXIS_RADIUS,
	}, {
		lambert: { map: texture },
	});
	wheel2.wheel.add(wheel2PivotAxis);

	// connector bar
	const connectorLength = Math.sqrt((wheel1Pivot.x - wheel2Pivot.x)**2 + (wheel1Pivot.z - wheel2Pivot.z)**2 );

	const midPoint = new THREE.Vector3();
	midPoint.lerpVectors(wheel1Pivot, wheel2Pivot, 0.5);

	const angleRadians = Math.atan2(wheel1Pivot.z - wheel2Pivot.z, wheel1Pivot.x - wheel2Pivot.x);

	const connector = scene3d.add.box({
		x: midPoint.x,
		y: midPoint.y,
		z: midPoint.z,
		depth: 0.1,
		height: 0.05,
		width: connectorLength + 0.1,
	}, {
		lambert: { map: texture },
	});
	connector.rotateY(-angleRadians);
	scene3d.physics.add.existing(connector, { mass: 0.1 });

	// hinge constraints
	scene3d.physics.add.constraints.hinge(wheel1.wheel.body, connector.body, {
		pivotA: {
			x: wheel1Pivot.x - wheel1.wheel.position.x,
			y: wheel1Pivot.y - wheel1.wheel.position.y,
			z: wheel1Pivot.z - wheel1.wheel.position.z },
		pivotB: { x: connectorLength / 2 },
		axisA: { y: 1 },
		axisB: { y: 1 }
	});

	scene3d.physics.add.constraints.hinge(wheel2.wheel.body, connector.body, {
		pivotA: {
			x: wheel2Pivot.x - wheel2.wheel.position.x,
			y: wheel2Pivot.y - wheel2.wheel.position.y,
			z: wheel2Pivot.z - wheel2.wheel.position.z },
		pivotB: { x: connectorLength / -2 },
		axisA: { y: 1 },
		axisB: { y: 1 }
	});

	return connector;
}

function createHingeWheel(
	scene3d: MainScene,
	floor: ExtendedObject3D,
	texture: THREE.Texture,
	position: THREE.Vector3,
	radius: number,
) {
	const { x, y, z } = position;

	const wheel = scene3d.add.cylinder({
		x,
		y,
		z,
		height: 0.1,
		radiusBottom: radius,
		radiusTop: radius,
	}, {
		lambert: { map: texture },
	});
	scene3d.physics.add.existing(wheel, { mass: 1 });

	// wheel axis
	scene3d.add.cylinder({
		x,
		y: y / 2,
		z,
		height: y,
		radiusBottom: AXIS_RADIUS,
		radiusTop: AXIS_RADIUS,
	}, {
		lambert: { map: texture },
	});

	const hinge = scene3d.physics.add.constraints.hinge(floor.body, wheel.body, {
		pivotA: { x, y, z },
		pivotB: { },
		axisA: { y: 1 },
		axisB: { y: 1 }
	});

	return { wheel, hinge };
}

function createObject(
	scene3d: MainScene,
	parent: ExtendedObject3D,
	texture: THREE.Texture,
	x: number,
	z: number,
) {
	const poleHeight = 8;

	// pole
	const pole = scene3d.add.cylinder({
		height: poleHeight,
		radiusBottom: POLE_RADIUS,
		radiusTop: POLE_RADIUS,
		x,
		y: poleHeight / 2,
		z,
	}, {
		lambert: { map: texture },
	});
	parent.add(pole);

	// plane
	const planeHeight = poleHeight - 4;
	const plane = scene3d.add.box({
		depth: 0.05,
		height: planeHeight,
		width: 2,
		x,
		y: poleHeight - (planeHeight / 2),
		z,
	}, {
		lambert: { map: texture },
	});
	parent.add(plane);
}

export default async function createObjects(projectSettings: ProjectSettings) {
	const { scene3d } = projectSettings;

	scene3d.cameraTarget.set(0, 1, 0);
	scene3d.pCamera.position.set(0, 2, 5);
	scene3d.pCamera.lookAt(scene3d.cameraTarget);
	scene3d.pCamera.updateProjectionMatrix();
	scene3d.orbitControls.target = scene3d.cameraTarget;
	scene3d.orbitControls.update();
	scene3d.orbitControls.saveState();

	// scene3d.physics.debug?.enable();

	const texture = await scene3d.load.texture('grid');

	const floor = scene3d.physics.add.box({
		collisionFlags: 1,
		depth: 10,
		height: 0.1,
		width: 10,
	}, { lambert: { map: texture } });

	scene3d.add.box({
		depth: 10,
		height: 10,
		width: 0.1,
		x: -5,
		y: 5,
	}, { lambert: { map: texture } });

	scene3d.add.box({
		depth: 0.1,
		height: 10,
		width: 10,
		y: 5,
		z: -5,
	}, { lambert: { map: texture } });

	createMachineA(scene3d, floor, texture);
	createMachineB(scene3d, floor, texture);
}

function createMachineA(scene3d: MainScene, floor: ExtendedObject3D, texture: THREE.Texture) {
	const wheel1Pos = new THREE.Vector3(0, 0.5, 0);
	const wheel2Pos = new THREE.Vector3(-2.4, 0.7, 0);
	const wheel3Pos = new THREE.Vector3(-1.5, 1.9, -1.7);
	const wheel4Pos = new THREE.Vector3(1.4, 0.7, -1.25);
	const wheel5Pos = new THREE.Vector3(0.1, 2.5, -2);

	const wheel1 = createHingeWheel(scene3d, floor, texture, wheel1Pos, 1);
	const wheel2 = createHingeWheel(scene3d, floor, texture, wheel2Pos, 1.5);
	const wheel3 = createHingeWheel(scene3d, floor, texture, wheel3Pos, 1);
	const wheel4 = createHingeWheel(scene3d, floor, texture, wheel4Pos, 1);
	const wheel5 = createHingeWheel(scene3d, floor, texture, wheel5Pos, 0.8);

	const wheel1Pivot = new THREE.Vector3(wheel1Pos.x, wheel2Pos.y + 0.2, wheel1Pos.z + 0.8);
	const wheel2Pivot = new THREE.Vector3(wheel2Pos.x + 0.4, wheel2Pos.y + 0.2, wheel2Pos.z + 1.2);
	const connector1 = createConnector(scene3d, texture, wheel1, wheel2, wheel1Pivot, wheel2Pivot);

	const wheel2Pivot2 = new THREE.Vector3(wheel2Pos.x + 0.7, wheel3Pos.y + 0.2, wheel2Pos.z - 0.2);
	const wheel3Pivot = new THREE.Vector3(wheel3Pos.x + 0.8, wheel3Pos.y + 0.2, wheel3Pos.z + 0.3);
	const connector2 = createConnector(scene3d, texture, wheel2, wheel3, wheel2Pivot2, wheel3Pivot);

	const wheel1Pivot2 = new THREE.Vector3(wheel1Pos.x, wheel4Pos.y + 0.2, wheel1Pos.z + 0.8);
	const wheel4Pivot = new THREE.Vector3(wheel4Pos.x, wheel4Pos.y + 0.2, wheel4Pos.z + 0.9);
	createConnector(scene3d, texture, wheel1, wheel4, wheel1Pivot2, wheel4Pivot);

	const wheel4Pivot2 = new THREE.Vector3(wheel4Pos.x - 0.2, wheel5Pos.y - 0.2, wheel4Pos.z + 0.2);
	const wheel5Pivot = new THREE.Vector3(wheel5Pos.x - 0.2, wheel5Pos.y - 0.2, wheel5Pos.z + 0.7);
	const connector4 = createConnector(scene3d, texture, wheel4, wheel5, wheel4Pivot2, wheel5Pivot);

	wheel1.hinge.enableAngularMotor(true, -0.8, 0.08);

	createObject(scene3d, wheel4.wheel, texture, 0.8, -0.3);
	createObject(scene3d, connector2, texture, 0.0, 0.0);
	createObject(scene3d, connector4, texture, 0.4, 0.0);
	createObject(scene3d, wheel2.wheel, texture, -0.3, 1.2);
}

function createMachineB(scene3d: MainScene, floor: ExtendedObject3D, texture: THREE.Texture) {
	const wheel1Pos = new THREE.Vector3(-0.7, 0.7, -1.6);
	const wheel2Pos = new THREE.Vector3(1.2, 1.1, 0.1);
	const wheel3Pos = new THREE.Vector3(-2.4, 1.3, 0);
	const wheel4Pos = new THREE.Vector3(0.3, 1.5, -1.35);

	const wheel1 = createHingeWheel(scene3d, floor, texture, wheel1Pos, 0.75);
	const wheel2 = createHingeWheel(scene3d, floor, texture, wheel2Pos, 1.0);
	const wheel3 = createHingeWheel(scene3d, floor, texture, wheel3Pos, 0.6);
	const wheel4 = createHingeWheel(scene3d, floor, texture, wheel4Pos, 0.55);

	const wheel1Pivot = new THREE.Vector3(wheel1Pos.x - 0.65, wheel2Pos.y + 0.2, wheel1Pos.z);
	const wheel2Pivot = new THREE.Vector3(wheel2Pos.x - 0.8, wheel2Pos.y + 0.2, wheel2Pos.z);
	createConnector(scene3d, texture, wheel1, wheel2, wheel1Pivot, wheel2Pivot);

	const wheel2Pivot2 = new THREE.Vector3(wheel2Pos.x, wheel3Pos.y + 0.2, wheel2Pos.z + 0.4);
	const wheel3Pivot = new THREE.Vector3(wheel3Pos.x, wheel3Pos.y + 0.2, wheel3Pos.z + 0.5);
	createConnector(scene3d, texture, wheel2, wheel3, wheel2Pivot2, wheel3Pivot);

	const wheel2Pivot3 = new THREE.Vector3(wheel2Pos.x + 0.3, wheel4Pos.y + 0.2, wheel2Pos.z - 0.4);
	const wheel4Pivot = new THREE.Vector3(wheel4Pos.x, wheel4Pos.y + 0.2, wheel4Pos.z + 0.4);
	const connector3 = createConnector(scene3d, texture, wheel2, wheel4, wheel2Pivot3, wheel4Pivot);

	wheel1.hinge.enableAngularMotor(true, 0.8, 0.08);

	createObject(scene3d, wheel2.wheel, texture, 0.6, 0.4);
	createObject(scene3d, wheel3.wheel, texture, -0.35, 0.35);
	createObject(scene3d, connector3, texture, 0.4, 0.0);
}

export function updateObjects() {
}
