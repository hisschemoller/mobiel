import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';

export type RobotLegsConfig = {
	limbDepth: number;
	limbWidth: number;
	lowerHeight: number;
	position: THREE.Vector3;
	rotateY: number;
	upperHeight: number;
	velocity: number;
	wheelRadius: number;
	wheelWidth: number;
}

export default class RobotLegs {

	constructor (asyncParam: boolean | undefined) {
		if (typeof asyncParam === 'undefined') {
			throw new Error('Cannot be called directly');
		}
	}

	static async build(
		projectSettings: ProjectSettings,
		texture: THREE.Texture,
		config: RobotLegsConfig,
	) {


		const instance = new RobotLegs(true);
		return instance;
	}
}
