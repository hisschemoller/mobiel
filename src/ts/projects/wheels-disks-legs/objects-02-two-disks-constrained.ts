import { ExtendedObject3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';

type Wheel = {
	hinge: Ammo.btHingeConstraint,
	wheel: ExtendedObject3D,
}

const AXIS_RADIUS = 0.05;

function createConnector(
	scene3d: MainScene,
	texture: THREE.Texture,
	wheel1: Wheel,
	wheel2: Wheel,
	wheel1Pivot: THREE.Vector3,
	wheel2Pivot: THREE.Vector3,
	) {
	// wheel1 axis
	const axis1Height = wheel1Pivot.y - wheel1.wheel.position.y;
	const wheel1PivotAxis = scene3d.add.cylinder({
		x: wheel1Pivot.x - wheel1.wheel.position.x,
		y: axis1Height / 2,
		z: wheel1Pivot.z - wheel1.wheel.position.z,
		height: axis1Height,
		radiusBottom: AXIS_RADIUS,
		radiusTop: AXIS_RADIUS,
	}, {
		lambert: { map: texture },
	});
	wheel1.wheel.add(wheel1PivotAxis);

	// wheel2 axis
	const axis2Height = wheel2Pivot.y - wheel2.wheel.position.y;
	const wheel2PivotAxis = scene3d.add.cylinder({
		x: wheel2Pivot.x - wheel2.wheel.position.x,
		y: axis2Height / 2,
		z: wheel2Pivot.z - wheel2.wheel.position.z,
		height: axis2Height,
		radiusBottom: AXIS_RADIUS,
		radiusTop: AXIS_RADIUS,
	}, {
		lambert: { map: texture },
	});
	wheel2.wheel.add(wheel2PivotAxis);

	// connector bar
	const connectorLength = Math.sqrt((wheel1Pivot.x - wheel2Pivot.x)**2 + (wheel1Pivot.y - wheel2Pivot.y)**2 );

	const midPoint = new THREE.Vector3();
	midPoint.lerpVectors(wheel1Pivot, wheel2Pivot, 0.5);

	const angleRadians = Math.atan2(wheel1Pivot.z - wheel2Pivot.z, wheel1Pivot.x - wheel2Pivot.x);

	const connector = scene3d.add.box({
		x: midPoint.x,
		y: midPoint.y,
		z: midPoint.z,
		depth: 0.1,
		height: 0.05,
		width: connectorLength + 0.1,
	}, {
		lambert: { map: texture },
	});
	connector.rotateY(-angleRadians);
	scene3d.physics.add.existing(connector, { mass: 0.1 });

	// hinge constraints
	scene3d.physics.add.constraints.hinge(wheel1.wheel.body, connector.body, {
		pivotA: {
			x: wheel1Pivot.x - wheel1.wheel.position.x,
			y: wheel1Pivot.y - wheel1.wheel.position.y,
			z: wheel1Pivot.z - wheel1.wheel.position.z },
		pivotB: { x: connectorLength / 2 },
		axisA: { y: 1 },
		axisB: { y: 1 }
	});

	scene3d.physics.add.constraints.hinge(wheel2.wheel.body, connector.body, {
		pivotA: {
			x: wheel2Pivot.x - wheel2.wheel.position.x,
			y: wheel2Pivot.y - wheel2.wheel.position.y,
			z: wheel2Pivot.z - wheel2.wheel.position.z },
		pivotB: { x: connectorLength / -2 },
		axisA: { y: 1 },
		axisB: { y: 1 }
	});
}

function createHingeWheel(
	scene3d: MainScene,
	floor: ExtendedObject3D,
	texture: THREE.Texture,
	position: THREE.Vector3,
	radius: number,
) {
	const { x, y, z } = position;

	const wheel = scene3d.add.cylinder({
		x,
		y,
		z,
		height: 0.1,
		radiusBottom: radius,
		radiusTop: radius,
	}, {
		lambert: { map: texture },
	});
	scene3d.physics.add.existing(wheel, { mass: 1 });

	// wheel axis
	scene3d.add.cylinder({
		x,
		y: y / 2,
		z,
		height: y,
		radiusBottom: AXIS_RADIUS,
		radiusTop: AXIS_RADIUS,
	}, {
		lambert: { map: texture },
	});

	const hinge = scene3d.physics.add.constraints.hinge(floor.body, wheel.body, {
		pivotA: { x, y, z },
		pivotB: { },
		axisA: { y: 1 },
		axisB: { y: 1 }
	});

	return { wheel, hinge };
}

export default async function createObjects(projectSettings: ProjectSettings) {
	const { scene3d } = projectSettings;

	// scene3d.physics.debug?.enable();

	const texture = await scene3d.load.texture('grid');

	const floor = scene3d.physics.add.box({
		collisionFlags: 1,
		depth: 10,
		height: 0.1,
		width: 10,
	}, { lambert: { map: texture } });

	const wheel1Pos = new THREE.Vector3(0, 0.5, 0);
	const wheel2Pos = new THREE.Vector3(-2.5, 0.7, 0);
	const wheel1Pivot = new THREE.Vector3(wheel1Pos.x - 0.9, wheel2Pos.y + 0.2, wheel2Pos.z);
	const wheel2Pivot = new THREE.Vector3(wheel2Pos.x - 0.9, wheel2Pos.y + 0.2, wheel2Pos.z);

	const wheel1 = createHingeWheel(scene3d, floor, texture, wheel1Pos, 2);
	const wheel2 = createHingeWheel(scene3d, floor, texture, wheel2Pos, 1);

	createConnector(scene3d, texture, wheel1, wheel2, wheel1Pivot, wheel2Pivot);

	wheel1.hinge.enableAngularMotor(true, -2, 0.1);
}

export function updateObjects() {

}
