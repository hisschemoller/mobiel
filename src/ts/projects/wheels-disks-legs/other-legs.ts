import { ExtendedObject3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';

export type OtherLegsConfig = {
	limbRadius: number;
	lowerHeight: number;
	position: THREE.Vector3;
	rotateY: number;
	upperHeight: number;
	velocity: number;
	wheelRadius: number;
	wheelWidth: number;
}

type OtherLegsParams = {
	body: ExtendedObject3D;
}

export default class OtherLegs {

	constructor (asyncParam: OtherLegsParams | undefined) {
		if (typeof asyncParam === 'undefined') {
			throw new Error('Cannot be called directly');
		}
	}

	static async build(
		projectSettings: ProjectSettings,
		texture: THREE.Texture,
		config: OtherLegsConfig,
	) {
		const { scene3d } = projectSettings;
		const {
			limbRadius,
			lowerHeight,
			position,
			rotateY,
			upperHeight,
			velocity,
			wheelRadius,
			wheelWidth,
		} = config;
		const axisRadius = 0.06;
		const limbGap = 0.1;
		const wheelToUpperDist = (wheelWidth * 0.5) + limbGap + (limbRadius * 0.5);
		const upperAxisOffset = (upperHeight * 0.5) - (limbRadius * 0.5);
		const upperToLowerDist = (limbRadius * 0.5) + limbGap + (limbRadius * 0.5);
		const lowerAxisOffset = (lowerHeight * 0.5) - (limbRadius * 0.5);

		const group = new THREE.Group();
		group.rotateY(rotateY);
		group.position.add(position);
		scene3d.scene.add(group);

		// WHEEL
		const wheel = scene3d.add.cylinder({
			x: 0,
			y: 0,
			z: 0,
			height: wheelWidth,
			radiusBottom: wheelRadius,
			radiusTop: wheelRadius,
		}, {
			lambert: { map: texture },
		});
		wheel.rotateZ(Math.PI * -0.5);
		group.add(wheel);
		scene3d.physics.add.existing(wheel, { mass: 0.1 });

		// UPPER LEFT
		const upperL = scene3d.add.cylinder({
			height: upperHeight,
			radiusTop: limbRadius,
    	radiusBottom: limbRadius,
		}, {
			lambert: { map: texture },
		});
		upperL.rotateX(Math.PI * 0.5);
		upperL.position.set(-wheelToUpperDist, 0, -upperAxisOffset);
		group.add(upperL);
		scene3d.physics.add.existing(upperL, { mass: 0.1 });

		scene3d.physics.add.constraints.hinge(wheel.body, upperL.body, {
			pivotA: { y: -wheelToUpperDist },
			pivotB: { y: upperAxisOffset },
			axisA: { y: -1 },
			axisB: { x: -1 },
		});

		// UPPER LEFT TO WHEEL AXIS
		const axisUpperLeft = scene3d.add.cylinder({
			height: wheelToUpperDist,
			radiusBottom: axisRadius,
			radiusTop: axisRadius,
		}, {
			lambert: { map: texture },
		});
		axisUpperLeft.position.set(wheelToUpperDist / 2, upperAxisOffset, 0);
		axisUpperLeft.rotateZ(Math.PI * 0.5);
		upperL.add(axisUpperLeft);

		// LOWER LEFT
		const lowerL = scene3d.add.cylinder({
			height: lowerHeight,
			radiusTop: limbRadius,
    	radiusBottom: limbRadius,
		}, {
			lambert: { map: texture },
		});
		lowerL.position.set(
			-(wheelToUpperDist + upperToLowerDist),
			-lowerAxisOffset,
			-(upperAxisOffset * 2),
		);
		group.add(lowerL);
		scene3d.physics.add.existing(lowerL, { mass: 0.1 });

		const motorL = scene3d.physics.add.constraints.hinge(upperL.body, lowerL.body, {
			pivotA: { x: -upperToLowerDist, y: -upperAxisOffset },
			pivotB: { y: lowerAxisOffset },
			axisA: { x: -1 },
			axisB: { x: -1 },
		});
		motorL.enableAngularMotor(true, velocity, velocity * 0.1);

		// LOWER LEFT TO UPPER LEFT AXIS
		const axisLowerLeft = scene3d.add.cylinder({
			height: upperToLowerDist,
			radiusBottom: axisRadius,
			radiusTop: axisRadius,
		}, {
			lambert: { map: texture },
		});
		axisLowerLeft.position.set(upperToLowerDist / 2, lowerAxisOffset, 0);
		axisLowerLeft.rotateZ(Math.PI * 0.5);
		lowerL.add(axisLowerLeft);

		// UPPER RIGHT
		const upperR = scene3d.add.cylinder({
			height: upperHeight,
			radiusTop: limbRadius,
    	radiusBottom: limbRadius,
		}, {
			lambert: { map: texture },
		});
		upperR.rotateX(Math.PI * -0.5);
		upperR.position.set(wheelToUpperDist, 0, upperAxisOffset);
		group.add(upperR);
		scene3d.physics.add.existing(upperR, { mass: 0.1 });

		scene3d.physics.add.constraints.hinge(wheel.body, upperR.body, {
			pivotA: { y: wheelToUpperDist },
			pivotB: { y: upperAxisOffset },
			axisA: { y: 1 },
			axisB: { x: 1 },
		});

		// UPPER RIGHT TO WHEEL AXIS
		const axisUpperRight = scene3d.add.cylinder({
			height: wheelToUpperDist,
			radiusBottom: axisRadius,
			radiusTop: axisRadius,
		}, {
			lambert: { map: texture },
		});
		axisUpperRight.position.set(wheelToUpperDist / -2, upperAxisOffset, 0);
		axisUpperRight.rotateZ(Math.PI * 0.5);
		upperR.add(axisUpperRight);

		// LOWER RIGHT
		const lowerR = scene3d.add.cylinder({
			height: lowerHeight,
			radiusTop: limbRadius,
    	radiusBottom: limbRadius,
		}, {
			lambert: { map: texture },
		});
		lowerR.position.set(
			(wheelToUpperDist + upperToLowerDist),
			-lowerAxisOffset,
			(upperAxisOffset * 2),
		);
		group.add(lowerR);
		scene3d.physics.add.existing(lowerR, { mass: 0.1 });

		const motorR = scene3d.physics.add.constraints.hinge(upperR.body, lowerR.body, {
			pivotA: { x: upperToLowerDist, y: -upperAxisOffset },
			pivotB: { y: lowerAxisOffset },
			axisA: { x: 1 },
			axisB: { x: 1 },
		});
		motorR.enableAngularMotor(true, velocity, velocity * 0.1);

		// LOWER RIGHT TO UPPER RIGHT AXIS
		const axisLowerRight = scene3d.add.cylinder({
			height: upperToLowerDist,
			radiusBottom: axisRadius,
			radiusTop: axisRadius,
		}, {
			lambert: { map: texture },
		});
		axisLowerRight.position.set(upperToLowerDist / -2, lowerAxisOffset, 0);
		axisLowerRight.rotateZ(Math.PI * 0.5);
		lowerR.add(axisLowerRight);
	}
}
