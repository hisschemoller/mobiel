import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';

export default class WheelDouble {

	constructor (asyncParam: boolean | undefined) {
		if (typeof asyncParam === 'undefined') {
			throw new Error('Cannot be called directly');
		}
	}

	static async build(
		projectSettings: ProjectSettings,
		texture: THREE.Texture,
		position: THREE.Vector3,
		rotationZ: number,
	) {
		const instance = new WheelDouble(true);
		instance.init(projectSettings, texture, position, rotationZ);

		return instance;
	}

	async init(
		projectSettings: ProjectSettings,
		texture: THREE.Texture,
		position: THREE.Vector3,
		rotateY: number,
	) {
		const { scene3d } = projectSettings;
		const axisWidth = 1.2;
		const wheelWidth = 0.1;
		const velocity = 1;
		const wheel1Radius = 1;
		const wheel2Radius = 0.8;

		const group = new THREE.Group();
		group.rotateY(rotateY);
		group.position.add(position);
		scene3d.scene.add(group);

		const axis = scene3d.add.cylinder({
			x: 0,
			height: axisWidth,
			radiusBottom: 0.1,
			radiusTop: 0.1,
		}, {
			lambert: { map: texture },
		});
		axis.rotateZ(Math.PI * 0.5);
		group.add(axis);
		scene3d.physics.add.existing(axis, { mass: 0.1 });

		const wheelLarge = scene3d.add.cylinder({
			x: ((axisWidth / 2) + (wheelWidth / 2)) * -1,
			height: wheelWidth,
			radiusBottom: wheel1Radius,
			radiusTop: wheel1Radius,
		}, {
			lambert: { map: texture },
		});
		wheelLarge.rotateZ(Math.PI * 0.5);
		group.add(wheelLarge);
		scene3d.physics.add.existing(wheelLarge, { mass: 0.1 });

		const wheelSmall = scene3d.add.cylinder({
			x: ((axisWidth / 2) + (wheelWidth / 2)),
			height: wheelWidth,
			radiusBottom: wheel2Radius,
			radiusTop: wheel2Radius,
		}, {
			lambert: { map: texture },
		});
		wheelSmall.rotateZ(Math.PI * 0.5);
		group.add(wheelSmall);
		scene3d.physics.add.existing(wheelSmall, { mass: 0.1 });

		const motor = scene3d.physics.add.constraints.hinge(wheelLarge.body, axis.body, {
			pivotA: { y: wheelWidth * -0.5 },
			pivotB: { y: axisWidth * 0.5 },
			axisA: { y: 1 },
			axisB: { y: 1 },
		});
		motor.enableAngularMotor(true, velocity, velocity * 0.1);

		scene3d.physics.add.constraints.fixed(wheelSmall.body, axis.body);
	}
}
