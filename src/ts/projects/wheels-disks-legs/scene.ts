import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
// import createObjects, { updateObjects } from './objects-01-doublewheels';
// import createObjects, { updateObjects } from './objects-02-two-disks-constrained';
// import createObjects, { updateObjects } from './objects-03-many-disks';
import createObjects, { updateObjects } from './objects-04-legs';

const BPM = 120;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 2;
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const STEP_DURATION = PATTERN_DURATION / STEPS;

export default class Scene extends MainScene {
	timeline: Timeline;

	constructor() {
		super();

		this.width = 1920;
		this.height = 1080;
		this.width3d = 16;
    this.height3d = (this.height / this.width) * this.width3d;
    this.shadowSize = 16;

    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });
	}

  preload() {
    this.load.preload('grid', '/assets/img/Blender-UVMap-Grid-1-K.jpg');
  }

	async create() {
    await super.create();

    // CAMERA
    this.cameraTarget.set(0, 0, 0);
    this.pCamera.position.set(0, 1.4, 5);
    this.pCamera.lookAt(this.cameraTarget);
    this.pCamera.updateProjectionMatrix();
    this.orbitControls.target = this.cameraTarget;
    this.orbitControls.update();
    this.orbitControls.saveState();

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.width * 0.75,
      height3d: this.width3d * 0.75,
      isPreview: false,
      measures: 1,
      patternDuration: PATTERN_DURATION,
      previewScale: 0.25,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

		await createObjects(projectSettings);

		this.postCreate();
	}

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    updateObjects();
    super.updateAsync(time, delta);
  }
}
