import { ExtendedObject3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';

export type DogConfig = {
	legHeight: number;
	limbDepth: number;
	limbWidth: number;
	position: THREE.Vector3;
	rotateY: number;
	velocity: number;
}

type DogParams = {
	body: ExtendedObject3D;
}

export default class Dog {

	constructor (asyncParam: DogParams | undefined) {
		if (typeof asyncParam === 'undefined') {
			throw new Error('Cannot be called directly');
		}
	}

	static async build(
		projectSettings: ProjectSettings,
		texture: THREE.Texture,
		config: DogConfig,
	) {
		const { scene3d } = projectSettings;
		const {
			legHeight,
			limbDepth,
			limbWidth,
			position,
			rotateY,
			velocity,
		} = config;
		const axisRadius = 0.06;
		const limbGap = 0.1;
		const bodyDepth = 3;
		const bodyWidth = 1;
		const bodyHeight = 0.1;
		const bodyToLegDist = (bodyWidth * 0.5) + limbGap + (limbWidth * 0.5);
		const legAxisOffset = (legHeight * 0.5) - (limbDepth * 0.5);
		const legDist = 1.3;

		const group = new THREE.Group();
		group.rotateY(rotateY);
		group.position.add(position);
		scene3d.scene.add(group);

		const body = scene3d.add.box({
			depth: bodyDepth,
			height: bodyHeight,
			width: bodyWidth,
		}, {
			lambert: { map: texture },
		});
		body.position.set(0, 0, 0);
		group.add(body);
		scene3d.physics.add.existing(body, { mass: 0.02 });

		// LEGS LEFT
		[-legDist, 0, legDist].forEach((z) => {
			const leg = scene3d.add.box({
				depth: limbDepth,
				height: legHeight,
				width: limbWidth,
			}, {
				lambert: { map: texture },
			});
			leg.position.set(-bodyToLegDist, -legAxisOffset, z);
			group.add(leg);
			scene3d.physics.add.existing(leg, { mass: 0.1 });

			const motorLeg = scene3d.physics.add.constraints.hinge(body.body, leg.body, {
				pivotA: { x: -bodyToLegDist, z },
				pivotB: { y: legAxisOffset },
				axisA: { x: -1 },
				axisB: { x: -1 },
			});
			motorLeg.enableAngularMotor(true, velocity * (0.8 + (Math.random() * 0.4)), velocity * 0.1);

			// UPPER LEFT TO WHEEL AXIS
			const axis = scene3d.add.cylinder({
				height: limbGap,
				radiusBottom: axisRadius,
				radiusTop: axisRadius,
			}, {
				lambert: { map: texture },
			});
			axis.position.set((limbWidth + limbGap) * 0.5, legAxisOffset , 0);
			axis.rotateZ(Math.PI * 0.5);
			leg.add(axis);
		});

		// LEGS RIGHT
		[-legDist, 0, legDist].forEach((z) => {
			const leg = scene3d.add.box({
				depth: limbDepth,
				height: legHeight,
				width: limbWidth,
			}, {
				lambert: { map: texture },
			});
			leg.position.set(bodyToLegDist, -legAxisOffset, z);
			group.add(leg);
			scene3d.physics.add.existing(leg, { mass: 0.1 });

			const motorLeg = scene3d.physics.add.constraints.hinge(body.body, leg.body, {
				pivotA: { x: bodyToLegDist, z },
				pivotB: { y: legAxisOffset },
				axisA: { x: 1 },
				axisB: { x: 1 },
			});
			motorLeg.enableAngularMotor(true, -velocity * (1.5 + (Math.random() * 0.4)), velocity * 0.1);

			// UPPER LEFT TO WHEEL AXIS
			const axis = scene3d.add.cylinder({
				height: limbGap,
				radiusBottom: axisRadius,
				radiusTop: axisRadius,
			}, {
				lambert: { map: texture },
			});
			axis.position.set((limbWidth + limbGap) * -0.5, legAxisOffset , 0);
			axis.rotateZ(Math.PI * 0.5);
			leg.add(axis);
		});

		const dog = new Dog({ body });
		return dog;
	}
}
