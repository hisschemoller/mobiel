import MainScene from '@app/mainscene';
import setup from '@app/app';
import scene from '@projects/wheels-disks-legs/scene';
import '../css/style.css';

setup(scene as unknown as MainScene);
