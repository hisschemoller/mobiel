import { defineConfig } from 'vite';
import path from 'path';

export default defineConfig({
  resolve: {
    alias: [
      {
        find: '@app',
        replacement: path.resolve(__dirname, 'src/ts/app'),
      },
      {
        find: '@projects',
        replacement: path.resolve(__dirname, 'src/ts/projects'),
      },
    ],
  },
});
