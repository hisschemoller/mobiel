# Mobiel

Onderwerpen

* Lopende en zelfrijdende apparaten.
* Machine met verbonden horizontale draaischijven.
* Zachte softbody voorwerpen.

## Projecten

### wheels-disks-legs

* objects-01-doublewheels
	* Twee wielen die met een as verbonden zijn en rondjes rijden.
* objects-02-two-disks-constrained
	* Twee horizontale schijven verbonden met een staaf.
* objects-03-many-disks
	* Complexe machine met veel verbonden schijven.
* objects-04-legs
	* Veel chaotische zelfbewegende machines met wielen en benen.

## Logo

Logo and icon inspiration:

![Workmates](public/assets/img/R-2216968-1472574445-5563.jpg 'Workmates')
